package task3.Task4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by rogovaya on 4/12/2016.
 */
public class Task4 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("please, input a number here:");
        String num1 = br.readLine();
        double num1Double = Double.parseDouble(num1);

        System.out.println("please, input sign here:");
        String sign = br.readLine();

        System.out.println("please, input a number here:");
        String num2 = br.readLine();
        double num2Double = Double.parseDouble(num2);

        double resultDouble = 0;
        boolean isSignNotFound = false;
        switch (sign) {
            case "+":
                resultDouble = num1Double + num2Double;
                break;
            case "-":
                resultDouble = num1Double - num2Double;
                break;
            case "*":
                resultDouble = num1Double * num2Double;
                break;
            case "/":
                resultDouble = num1Double / num2Double;
                break;
            default:  {
                isSignNotFound = true;
            }
        }
        if(isSignNotFound == true){
            System.out.println("Недопустимый символ");
        } else {
            int resultInt = (int) resultDouble;
            if(resultInt == resultDouble){
                System.out.println("result is " + resultInt);
            } else {
                System.out.println("result is " + resultDouble);
            }
        }
        }
    }