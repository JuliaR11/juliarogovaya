package test1.arithmetic.task2;

/**
 * Created by rogovaya on 4/11/2016.
 */
public class task2 {
    public static void main (String args [])
    {
        int a = 5;
        String s = Integer.toString(a);
        System.out.println(s+1);
        Integer a1 = Integer.valueOf(s);
        System.out.println(a1+1);

        double b = 2.5;
        String d = Double.toString(b);
        System.out.println(d+1);
        Double b1 = Double.valueOf(d);
        System.out.println(b1+1);

        long y = 40000;
        String w = Long.toString(y);
        System.out.println(w+1);
        Long y1 = Long.valueOf(w);
        System.out.println(y1+1);

        float j = 2.48f;
        String f = Float.toString(j);
        System.out.println(f+1);
        Float j1 = Float.valueOf(f);
        System.out.println(j1+1);

        byte x = 8;
        String m = Byte.toString(x);
        System.out.println(m+1);
        Byte x1 = Byte.valueOf(m);
        System.out.println(x1+1);

        char c = 'Q';
        String k = Character.toString(c);
        System.out.println(k+1);
        char ch1 = k.charAt(0);
        System.out.println(ch1);

        short n = 12;
        String h = Short.toString(n);
        System.out.println(h+1);
        Short n1 = Short.valueOf(h);
        System.out.println(n1+1);

        boolean bool = true;
        String v = Boolean.toString(bool);
        System.out.println(v+1);
        Boolean bool1 = Boolean.valueOf(v);
        System.out.println(bool1);


    }
}
