/**
 * Created by rogovaya on 4/13/2016.
 */
public class Bool {
    public static void main(String args[]) {
        boolean a = true;
        boolean b = false;

        boolean x = a && b;
        boolean y = a || b;
        boolean w = !(a && b);

        System.out.println(x);
        System.out.println(y);
        System.out.println(w);

    }
}
